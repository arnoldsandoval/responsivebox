# ResponsiveBox

A jQuery plugin that allows you to easily create responsive lightboxes.

## Getting Started
Download the [production version][min] or the [development version][max].

[min]: https://raw.github.com/arnold/new/master/dist/ResponsiveBox.min.js
[max]: https://raw.github.com/arnold/new/master/dist/ResponsiveBox.js

In your web page:

```html
<script src="jquery.js"></script>
<script src="dist/jquery.responsive-box.js"></script>
<link rel="stylesheet" type="text/css" href="jquery.responsive-box.css"></link>
<script>
jQuery(function($) {
 $(document).ResponsiveBox();
});
</script>
```

Add the data-rb attribute to the element that will open your box.

###Image example
```
<button data-rb="image:{URL}">Click me!</button>
```

###YouTube example
```
<button data-rb="youtube:{YOUTUBE_ID}">Watch me!</button>
```

###AJAX example
```
<button data-rb="ajax:{URL_TO_LOAD}">AJAX me!</button>
```

That's it! You're done.

You can effortlessly customize the box by adding to the data-rb property. For instance, if you want to change the event form the default click you can do this:
```
<button data-rb="image:{URL} event:hover">…</button>
```

You can also quickly add in effects:
```
<button data-rb="image:{URL} event:hover">…</button>
```

## Release History
_(Nothing yet)_

## To Do
  * Get events join
* Use YouTube API?
