/*! ResponsiveBox - v0.1.0 - 2014-01-20
* https://github.com/arnoldsandoval/responsive-box
* Copyright (c) 2014 Arnold Sandoval; Licensed MIT */
/*! ResponsiveBox - v0.1.0 - 2014-01-20
* https://github.com/arnoldsandoval/responsive-box
* Copyright (c) 2014 Arnold Sandoval; Licensed MIT */
/*!
 * ResponsiveBox
 * Original author: @arnoldsandoval
 * Licensed under the WTFPL (http://www.wtfpl.net)
 */

window.log = function () {
  window.log.history = window.log.history || [];   // store logs to an array for reference
  window.log.history.push(arguments);
  if(this.console){
    window.console.log( Array.prototype.slice.call(arguments) );
  }
};

;(function ($, window, document) {

    'use strict';

    // usage: log('inside coolFunc',this,arguments);
    // http://paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/



    var pluginName = "ResponsiveBox";
        // defaults = {
        //     type: "image", // image, video, ajax
        //     src: null
        // };

    // The actual plugin constructor
    function Plugin() {
        // this.element = element;
        


        // jQuery has an extend method that merges the
        // contents of two or more objects, storing the
        // result in the first object. The first object
        // is generally empty because we don't want to alter
        // the default options for future instances of the plugin
        //this.options = $.extend( {}, defaults, options) ;
        //this._defaults = defaults;
        this._name = pluginName;
        
        //this.$box_element = $(this.options.boxElement).children();
        this.doc_height = document.height;        
        
        
        this.init();

    }

    Plugin.prototype = { 
        properties: [],
        isOpen: false,
        checkTransform: function(){
            var el = document.createElement('p'),
            has3d,
            transforms = {
                'webkitTransform':'-webkit-transform',
                'OTransform':'-o-transform',
                'msTransform':'-ms-transform',
                'MozTransform':'-moz-transform',
                'transform':'transform'
            };

            // Add it to the body to get the computed style
            document.body.insertBefore(el, null);

            for(var t in transforms){
                if( el.style[t] !== undefined ){
                    el.style[t] = 'translate3d(1px,1px,1px)';
                    has3d = window.getComputedStyle(el).getPropertyValue(transforms[t]);
                }
            }

            document.body.removeChild(el);

            return (has3d !== undefined && has3d.length > 0 && has3d !== "none");
        },

        init: function() {
            
            window.log('checkTransform: '+this.checkTransform());

            var self = this;

            $("[data-rb]").each(function(){
                
                var attributes =  $(this).attr('data-rb'),
                    box_properties = self.parseAttribute(attributes, " ");

                // self.checkKey();            
                self.properties.push(box_properties);

                self.createBox(box_properties);

            });

            window.log(self.properties);           

            if($(".rb-overlay").length <= 0){
                $('body').append('<div class="rb-overlay"></div>');
            }


            // $('[data-rb]').on('click', function(){

//open
            //     var index = $('[data-rb]').index(this);
            //     window.log(self.properties[index]);
            //     $('.rb-modal').removeClass('is-open');
            //     $('.rb-modal').eq(index).addClass('is-open');

            //     // self.position();                
            // });
//close
            self.eventHandlers();

        },
        loadAjax: function(args){

            // var self = this;
            // $('[data-rb]').index(self);

            window.log(args.el);

            $(args.el).find('.rb-content > div').delay(5000).html('<div "text-align:center;background:red;">Loading&hellip;</div>').load(args.url);

        },
        modalOpen: function(index){
            
          var self = this,
              isAjax = self.checkKey('ajax', index);
          
            this.isOpen = true;

            if(isAjax === true){

                var dataUrl = self.properties[index].ajax;

                self.loadAjax({
                    url: dataUrl,
                    el: $('.rb-modal').eq(index)
                }, index);
            }
          

            $('.rb-modal').removeClass('is-open');
            $('.rb-modal').eq(index).addClass('is-open');
            
            if(self.checkTransform() === false){

                var modalHeight = $('.rb-modal.is-open').outerHeight(),
                    modalWidth = $('.rb-modal.is-open').outerWidth();
                 $('.rb-modal.is-open').css({
                    'border': '21px solid red',
                    'top': '50%',
                    'left': '50%',
                    'margin-top': -Math.abs(modalHeight/2)+'px',
                    'margin-left': -Math.abs(modalWidth/2)+'px'
                 });
            }

        },
        modalClose: function(){
            
            this.isOpen = false;

            if($(".rb-modal.is-open").length >= 0){
            
                $('.rb-overlay, .rb-close').on('click', function(){
                    $('.rb-modal').removeClass('is-open');                    
                    // self.position();                
                });
            }

        },    
        eventHandlers: function(){
            var self = this;

            $('[data-rb]').on('click', function(e){
                var index = $('[data-rb]').index(this);
                self.modalOpen(index);
                e.preventDefault();
            });

            self.modalClose(this);

        },        
        checkKey: function(key, index){
            
            var self = this;
            
            if(key in self.properties[index]){
                window.log(key);
                return true;
            }

        },
        parseAttribute: function(str, separator){
            var parsed = {};
            var pairs = str.split(separator);
            for (var i = 0, len = pairs.length, keyVal; i < len; ++i) {
                keyVal = pairs[i].split(/:(.+)?/);
                if (keyVal[0]) {
                    parsed[keyVal[0]] = keyVal[1];
                }
            }
            return parsed;
        },
        createBox: function(properties){

            if('image' in properties){
                $('body').append('<div class="rb-modal type-image effect-'+properties.effect+'"><div class="rb-content"><button class="rb-close">X</button><img src="'+properties.image+'"></div></div>');                                                      
            }
            else if('youtube' in properties){
                $('body').append('<div class="rb-modal type-youtube effect-'+properties.effect+'"><div class="rb-content"><button class="rb-close">X</button><iframe id="ytplayer" type="text/html" src="http://www.youtube.com/embed/'+properties.youtube+'" frameborder="0"/></iframe></div></div>');                                                      
            } else if('ajax' in properties){
                $('body').append('<div class="rb-modal type-ajax effect-'+properties.effect+'"><div class="rb-content"><button class="rb-close">X</button><div>'+properties.ajax+'</div></div></div>');
            } else if('iframe' in properties){
                $('body').append('<div class="rb-modal type-iframe effect-'+properties.effect+'"><div class="rb-content"><button class="rb-close">X</button><div><iframe src="'+properties.iframe+'"></iframe></div></div></div>');
            } else{
             //   throw new Error('ResponsiveBox: Check your [data-rb] and ensure that you are passing content (image, youtube or ajax)');
            }

        },
        // box_size: function(el, options) {
        //     // some logic
        // },
        // position: function() {
        //     // some logic

        //     // this.box_height = this.$box_element.children().height();
        //     // this.box_width = this.$box_element.children().width();


        //     // // this.$box_element.parent().css({
        //     // //     'height': this.doc_height                
        //     // // });

            
        //     // this.$box_element.css({
        //     //     'margin-top': this.box_height/2 * -1,
        //     //     'margin-left': this.box_width/2 * -1
        //     // });
            
        // },        
    };

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function ( options ) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName,
                new Plugin( this, options ));
            }
        });
    };

})( jQuery, window, document );